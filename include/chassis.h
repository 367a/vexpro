/** @file chassis.h
 * @brief Header file for chassis drive control
 */

#ifndef _CHASSIS_H_
#define _CHASSIS_H_

#include "API.h"

/**
 * Drive method for Mecanum platform.
 *
 * Angles are measured clockwise from the positive X axis. The robot's speed is
 * independent from its angle or rotation rate.
 *
 * @param ySpeed    The robot's speed along the Y axis [-1.0..1.0]. Right is
 *                  positive.
 * @param xSpeed    The robot's speed along the X axis [-1.0..1.0]. Forward is
 *                  positive.
 * @param zRotation The robot's rotation rate around the Z axis [-1.0..1.0].
 *                  Clockwise is positive.
 * @param gyroAngle The current angle reading from the gyro in degrees around
 *                  the Z axis. Use this to implement field-oriented controls.
 */
void chassisDriveCartesian(double x, double y, double rotation, double gyroAngle);

/**
 * Drive method for Mecanum platform.
 *
 * Angles are measured clockwise from the positive X axis. The robot's speed is
 * independent from its angle or rotation rate.
 *
 * @param magnitude The robot's speed at a given angle [-1.0..1.0]. Forward is
 *                  positive.
 * @param angle     The angle around the Z axis at which the robot drives in
 *                  degrees [-180..180].
 * @param zRotation The robot's rotation rate around the Z axis [-1.0..1.0].
 *                  Clockwise is positive.
 */
void chassisDrivePolar(double magnitude, double angle, double rotation);

/**
 * @brief      Reads the value of the gyroscopic sensor
 */
int chassisGyroGet(void);

/**
 * @brief      Resets the value of the gyroscopic sensor
 */
void chassisGyroReset(void);

void chassisUseGyro(bool enabled);

/**
 * @brief      Printf debug info on the serial port. Called every 20 ticks to keep from flooding serial channel.
 */
void chassisDebug(void);

/**
 * @brief      All code for commanding robot hardware is contained in the update method.
 */
void chassisUpdate(void);

/**
 * @brief      Called during init to setup chassis related motors/sensors/encoders.
 */
void chassisInit(void);

#endif // _CHASSIS_H_
