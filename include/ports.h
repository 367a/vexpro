/** @file ports.h
 * @brief Header file for defining motor/sensor ports as well as sensor and power minimums and maximums
 */

#ifndef _PORTS_H_
#define _PORTS_H_

// MOTOR DEFINITIONS
#define CHASSIS_FRONT_LEFT     2
#define CHASSIS_FRONT_RIGHT    3
#define CHASSIS_REAR_LEFT      4
#define CHASSIS_REAR_RIGHT     5
#define CLAW_MOTOR             6
#define EXTEND_MOTOR           7
#define WRIST_MOTOR            8
#define LIFT_MOTOR             9

//Sensors
#define LIFT_ENCODER_TOP       1
#define LIFT_ENCODER_BOTTOM    2
#define WRIST_ENCODER_TOP      3
#define WRIST_ENCODER_BOTTOM   4

#define EXTEND_POTEN           8
#define CLAW_POTEN             7
#define GYRO_PORT              1
#define LCD_PORT uart1

//Limits
#define LIFT_ENC_MIN 30
#define LIFT_ENC_MAX 550
#define LIFT_POWER_MAX 127
#define LIFT_POWER_MIN 90

#define EXTEND_POT_MAX 4025
#define EXTEND_POT_MIN 660
#define EXTEND_POWER_MAX 50
#define EXTEND_POWER_MIN 50

#define WRIST_ENC_MAX 1300
#define WRIST_ENC_MIN 100
#define WRIST_POWER_MIN 110
#define WRIST_POWER_MAX 110

#define CLAW_POT_MAX 2060
#define CLAW_POT_MIN 830
#define CLAW_POWER_MAX 40
#define CLAW_POWER_MIN 40

#endif /* end of include guard: _PORTS_H_ */
