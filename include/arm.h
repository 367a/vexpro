/** @file arm.h
 * @brief Header file for robot arm control
 */
#ifndef _ARM_H_
#define _ARM_H_

#include "API.h"

/**
 * Specifiers for the various joints on the arm
 */
typedef enum {
	LIFT,
	EXTEND,
	WRIST,
	CLAW
} ArmJoint;

/**
 * @brief      Printf debug info related to the arm
 */
void armDebug(void);

/**
 * @brief      Initialize all variables associated with the arm
 */
void armInit(void);

/**
 * @brief      All interaction with the robot hardware happens in this method
 */
void armUpdate(void);

/**
 * @brief      Sets the motor power of the specified joint
 *
 * @param[in]  joint  The joint to set
 * @param[in]  power  The power to set
 */
void armSet(ArmJoint joint, int power);

/**
 * @brief      Gets the sensor value of the specified joint
 *
 * @param[in]  joint  The joint to read
 *
 * @return     The sensor value of the specified joint
 */
int armGet(ArmJoint joint);

/**
 * @brief      Modifies pid settings for the specified joint
 *
 * @param[in]  joint  The joint to change
 * @param[in]  p      delta kP
 * @param[in]  i      delta kI
 * @param[in]  d      delta kD
 */
void armModPid(ArmJoint joint, double p, double i, double d);

/**
 * @brief      Sets the enabled/disabled state for a specified joints pid controller
 *
 * @param[in]  joint    The joint to change
 * @param[in]  enabled  If pid should be enabled
 */
void armSetPid(ArmJoint joint, bool enabled);

/**
 * @brief      Gets if the specified joint is using pid control or not
 *
 * @param[in]  joint  The joint to check
 *
 * @return     If pid is enabled for that joint
 */
bool armGetPid(ArmJoint joint);

/**
 * @brief      Sets the pid controller target for a specified joint
 *
 * @param[in]  joint   The joint to change
 * @param[in]  target  The target sensor value to seek
 */
void armSetTarget(ArmJoint joint, int target);

/**
 * @brief      Gets the pid controller target for a specified joint
 *
 * @param[in]  joint  The joint to read
 *
 * @return     The target sensor value being seeked
 */
int armGetTarget(ArmJoint joint);


#endif // _ARM_H_
