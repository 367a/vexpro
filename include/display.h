/** @file display.h
 * @brief Header file for robot lcd control
 */
#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "API.h"

#define APP_COUNT 16
#define LINE_LEN 16
#define LINE_COUNT 2
#define LINE_BUFFER (LINE_LEN * LINE_COUNT)

typedef bool(*AppUpdate)(void);
typedef void(*AppPreview)(void);

/**
 * @brief      Registers a new function for the display
 *
 * @param[in]  app_preview  Callback to a function that provides a one line preview of state
 * @param[in]  app_func     Callback to a function with exclusive control over the display
 */
void displayRegisterApp(AppPreview app_preview, AppUpdate app_func);

/**
 * @brief      All display interaction with robot hardware happens in this function
 */
void displayUpdate(void);

/**
 * @brief      Initializes display
 */
void displayInit(void);

#endif // _CHASSIS_H_
