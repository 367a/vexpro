#include "main.h"
#include "chassis.h"

void vec2Rotate(double *x, double *y, double angle);
void normalize(double motors[4]);

static unsigned char ports[4] = {CHASSIS_FRONT_LEFT, CHASSIS_FRONT_RIGHT, CHASSIS_REAR_LEFT, CHASSIS_REAR_RIGHT};
Gyro gyro;

static struct {
	double speed[4];
	bool useGyro;
} chassis = {{0, 0, 0, 0}, true};

void chassis_app_preview(void) {
	lcdPrint(LCD_PORT, 1, "Chassis Gyro");
	lcdPrint(LCD_PORT, 2, "%d", gyroGet(gyro));
}

bool chassis_app_update(void) {
	lcdPrint(LCD_PORT, 1, "%4d,%4d - %4d", (int) chassis.speed[0],  (int)chassis.speed[1], gyroGet(gyro));
	lcdPrint(LCD_PORT, 1, "%4d,%4d",  (int)chassis.speed[2], (int) chassis.speed[3]);
	return buttonIsNewPress(LCD_CENT);
}

//Sets the motor ports of the base chassis
void chassisInit(void) {
	gyro = gyroInit(GYRO_PORT, 0);
	blrsMotorInit(CHASSIS_FRONT_LEFT, false, 3.0, NULL);
	blrsMotorInit(CHASSIS_FRONT_RIGHT, false, 3.0, NULL);
	blrsMotorInit(CHASSIS_REAR_LEFT, false, 3.0, NULL);
	blrsMotorInit(CHASSIS_REAR_RIGHT, false, 3.0, NULL);
	for(int i = 0; i < 4; i++) {
		chassis.speed[i] = 0;
	}
	chassisUseGyro(true);
	displayRegisterApp(chassis_app_preview, chassis_app_update);
}

int chassisGyroGet(void) {
	return gyroGet(gyro);
}

void chassisDrivePolar(double magnitude, double angle, double r) {
	chassisDriveCartesian(magnitude * cos(angle), magnitude * sin(angle), r, gyroGet(gyro));
}

void chassisDriveCartesian(double x, double y, double r, double gyroAngle) {
	if(chassis.useGyro) {
		vec2Rotate(&x, &y, gyroAngle);
	}

	chassis.speed[0] =  x + y + r;
	chassis.speed[1] =  x - y + r;
	chassis.speed[2] = -x + y + r;
	chassis.speed[3] = -x - y + r;
	normalize(chassis.speed);
}

void chassisDebug(void) {
	printf("drive - %4d,%4d,%4d,%4d\n", (int) chassis.speed[0], (int) chassis.speed[1], (int) chassis.speed[2], (int) chassis.speed[3]);
}

void chassisUpdate(void) {
	for(int i = 0; i < 4; i++) {
		blrsMotorSet(ports[i], chassis.speed[i], false);
	}
}

void chassisGyroReset(void) {
	gyroReset(gyro);
}

void chassisUseGyro(bool enabled) {
	chassis.useGyro = enabled;
}

void vec2Rotate(double *x, double *y, double angle) {
  double cosA = cos(angle * (PI / 180.0));
  double sinA = sin(angle * (PI / 180.0));
  double out[2];
  out[0] = (*x) * cosA - (*y) * sinA;
  out[1] = (*x) * sinA + (*y) * cosA;
  *x = out[0];
  *y = out[1];
}

void normalize(double motors[4]) {
	double max = 0;
	for(int i = 0; i < 4; i++) { max = fmax(max, abs(motors[i])); }
	for(int i = 0; i < 4; i++) { motors[i] = (motors[i]/max) * fmin(max, 127.0); }
}
