#include "main.h"
#include "display.h"

typedef struct {
	bool exists;
	AppPreview preview;
	AppUpdate update;
} App;
App apps[APP_COUNT] = {{false, NULL, NULL}};
App *focus = NULL;
int app_idx = 0;

void displayRegisterApp(AppPreview app_preview, AppUpdate app_func) {
	int i = 0;
	while(i < APP_COUNT && apps[i].exists) {
		i++;
	}
	if(i < APP_COUNT) {
		apps[i].exists = true;
		apps[i].preview = app_preview;
		apps[i].update = app_func;
	}
}

void displayUpdate(void) {
	if(focus == NULL) {
		if(buttonIsNewPress(LCD_CENT))  focus = &apps[app_idx];
		if(buttonIsNewPress(LCD_LEFT))  app_idx = (app_idx + (APP_COUNT - 1)) % APP_COUNT;
		if(buttonIsNewPress(LCD_RIGHT)) app_idx = (app_idx + 1) % APP_COUNT;
		if(apps[app_idx].exists) apps[app_idx].preview();
	} else {
		if(focus->update()) {
			focus = NULL;
		}
	}
}

void displayInit(void) {
	lcdInit(LCD_PORT);
	lcdClear(LCD_PORT);
}
