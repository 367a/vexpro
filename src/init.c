/** @file init.c
 * @brief File for initialization code
 *
 * This file should contain the user initialize() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"

/*
 * Runs pre-initialization code. This function will be started in kernel mode one time while the
 * VEX Cortex is starting up. As the scheduler is still paused, most API functions will fail.
 *
 * The purpose of this function is solely to set the default pin modes (pinMode()) and port
 * states (digitalWrite()) of limit switches, push buttons, and solenoids. It can also safely
 * configure a UART port (usartOpen()) but cannot set up an LCD (lcdInit()).
 */
void initializeIO() {
  //standaloneModeEnable();
  //usartInit(uart1, 115200, 0);
}

/*
 * Runs user initialization code. This function will be started in its own task with the default
 * priority and stack size once when the robot is starting up. It is possible that the VEXnet
 * communication link may not be fully established at this time, so reading from the VEX
 * Joystick may fail.
 *
 * This function should initialize most sensors (gyro, encoders, ultrasonics), LCDs, global
 * variables, and IMEs.
 *
 * This function must exit relatively promptly, or the operatorControl() and autonomous() tasks
 * will not start. An autonomous mode selection menu like the pre_auton() in other environments
 * can be implemented in this task if desired.
 */
void initialize() {
	motorManagerInit();
	displayInit();
	chassisInit();
	armInit();
}

double constrain(double value, double min_, double max_) {
	return fmax(min_, fminf(value, max_));
}

double scale(double x, double min, double max) {
	if(x < 0) {
		return (x/127.0) * min;
	} else {
		return (x/127.0) * max;
	}
}

double limit(double value) {
	if (value > 127.0) {
		return 127.0;
	}
	if (value < -127.0) {
		return -127.0;
	}
	return value;
}

double applyDeadband(double value, double deadband) {
	double result = 0;
	if (abs(value) > deadband) {
		if (value > 0.0) {
			result = (value - deadband) / (127.0 - deadband);
		} else {
			result = (value + deadband) / (127.0 - deadband);
		}
	}
	return result * 127.0;
}
