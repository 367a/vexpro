#include "main.h"
#include "arm.h"

struct Joint_s {
	fbc_t fbc;
	fbc_pid_t pid;
	bool pidEnabled;
	Encoder encoder;
	int power;
	int sensor;
};

typedef struct Joint_s Joint;

static Joint joints[4];
Joint *lift = &joints[LIFT];
Joint *extend = &joints[EXTEND];
Joint *wrist = &joints[WRIST];
Joint *claw = &joints[CLAW];

void liftSet(int x) {
	lift->power = x;
}

int liftGet(void) {
	return lift->sensor;
}

void extendSet(int x) {
	extend->power = x;
}

int extendGet(void) {
	return extend->sensor;
}

void wristSet(int x) {
	wrist->power = x;
}

int wristGet(void) {
	return wrist->sensor;
}

void clawSet(int x) {
	claw->power = x;
}

int clawGet(void) {
	return claw->sensor;
}

void app_lift_preview(void) {
	lcdPrint(LCD_PORT, 1, "Arm Lift PID");
	lcdPrint(LCD_PORT, 2, "%d %d", lift->sensor, lift->power);
}

void app_extend_preview(void) {
	lcdPrint(LCD_PORT, 1, "Arm Extend PID");
	lcdPrint(LCD_PORT, 2, "%d %d", extend->sensor, extend->power);
}

void app_wrist_preview(void) {
	lcdPrint(LCD_PORT, 1, "Arm Wrist PID");
	lcdPrint(LCD_PORT, 2, "%d %d", wrist->sensor, wrist->power);
}

void app_claw_preview(void) {
	lcdPrint(LCD_PORT, 1, "Arm Claw PID");
	lcdPrint(LCD_PORT, 2, "%d %d", claw->sensor, claw->power);
}

bool app_lift_update(void) {
	lcdPrint(LCD_PORT, 1, "%.2f|%.5f|%.2f", lift->pid.kP, lift->pid.kI, lift->pid.kD);
	lcdPrint(LCD_PORT, 2, "%d|%lu", (lift->fbc.goal-liftGet()), lift->pid._integral);
	return buttonIsNewPress(LCD_CENT);
}

bool app_extend_update(void) {
	lcdPrint(LCD_PORT, 1, "%.2f|%.5f|%.2f", extend->pid.kP, extend->pid.kI, extend->pid.kD);
	lcdPrint(LCD_PORT, 2, "%d|%lu", (extend->fbc.goal-liftGet()), extend->pid._integral);
	return buttonIsNewPress(LCD_CENT);
}

bool app_wrist_update(void) {
	lcdPrint(LCD_PORT, 1, "%.2f|%.5f|%.2f", wrist->pid.kP, wrist->pid.kI, wrist->pid.kD);
	lcdPrint(LCD_PORT, 2, "%d|%lu", (wrist->fbc.goal-liftGet()), wrist->pid._integral);
	return buttonIsNewPress(LCD_CENT);
}

bool app_claw_update(void) {
	lcdPrint(LCD_PORT, 1, "%.2f|%.5f|%.2f", claw->pid.kP, claw->pid.kI, claw->pid.kD);
	lcdPrint(LCD_PORT, 2, "%d|%lu", (claw->fbc.goal-liftGet()), claw->pid._integral);
	return buttonIsNewPress(LCD_CENT);
}

void armInit(void) {
	displayRegisterApp(&app_lift_preview, &app_lift_update);
	displayRegisterApp(&app_extend_preview, &app_extend_update);
	displayRegisterApp(&app_wrist_preview, &app_wrist_update);
	displayRegisterApp(&app_claw_preview, &app_claw_update);

	lift->power = 0;
	lift->sensor = 0;
	lift->encoder = encoderInit(LIFT_ENCODER_TOP, LIFT_ENCODER_BOTTOM, false);
 	blrsMotorInit(LIFT_MOTOR, true, 2.0, NULL);
	fbcInit(&lift->fbc, &liftSet, &liftGet, NULL, fbcStallDetect, -25, 25, 60, 30);
	fbcPIDInitializeData(&lift->pid, 4, 0, 0, -200, 200);
	fbcPIDInit(&lift->fbc, &lift->pid);
	lift->pidEnabled = false;

	//extend
	extend->power = 0;
	extend->sensor = 0;
	blrsMotorInit(EXTEND_MOTOR, false, 2.0, NULL);
	fbcInit(&extend->fbc, &extendSet, &extendGet, NULL, fbcStallDetect, -30, 30, 60, 30);
	fbcPIDInitializeData(&extend->pid, .5, .2, 1, -500, 500);
	fbcPIDInit(&extend->fbc, &extend->pid);
	extend->pidEnabled = false;

	//wrist
	wrist->power = 0;
	wrist->sensor = 0;
	wrist->encoder = encoderInit(WRIST_ENCODER_TOP, WRIST_ENCODER_BOTTOM, true);
	blrsMotorInit(WRIST_MOTOR, true, 2.0, NULL);
	fbcInit(&wrist->fbc, &wristSet, &wristGet, NULL, fbcStallDetect, -30, 30, 60, 30);
	fbcPIDInitializeData(&wrist->pid, 3, 0, 0, -200, 200);
	fbcPIDInit(&wrist->fbc, &wrist->pid);
	wrist->pidEnabled = false;

	claw->power = 0;
	claw->sensor = 0;
	blrsMotorInit(CLAW_MOTOR, false, 2.0, NULL);
	fbcInit(&claw->fbc, &clawSet, &clawGet, NULL, fbcStallDetect, -30, 30, 60, 30);
	fbcPIDInitializeData(&claw->pid, 1, 0, 0, -30, 30);
	fbcPIDInit(&claw->fbc, &claw->pid);
	claw->pidEnabled = false;
}

void armDebug(void) {
		printf("%4d,%4d\n", lift->sensor, lift->power);
		printf("%4d,%4d\n", extend->sensor, extend->power);
		printf("%4d,%4d\n", wrist->sensor, wrist->power);
		printf("%4d,%4d\n", claw->sensor, claw->power);
}

void armUpdate(void) {
	lift->sensor = encoderGet(lift->encoder);
	extend->sensor = analogRead(EXTEND_POTEN);
	wrist->sensor = encoderGet(wrist->encoder);
	claw->sensor = analogRead(CLAW_POTEN);

	if(lift->pidEnabled) {
		fbcRunContinuous(&lift->fbc);
	}
	if(extend->pidEnabled) {
		fbcRunContinuous(&extend->fbc);
	}
	if(wrist->pidEnabled) {
		fbcRunContinuous(&wrist->fbc);
	}
	if(claw->pidEnabled) {
		fbcRunContinuous(&claw->fbc);
	}

	if(lift->sensor > LIFT_ENC_MAX && lift->power > 0) {lift->power = 0;}
	if(lift->sensor < LIFT_ENC_MIN && lift->power < 0) {lift->power = 0;}
	if(extend->sensor > EXTEND_POT_MAX && extend->power > 0) {extend->power = 0;}
	if(extend->sensor < EXTEND_POT_MIN && extend->power < 0) {extend->power = 0;}
	if(wrist->sensor > WRIST_ENC_MAX && wrist->power > 0) {wrist->power = 0;}
	if(wrist->sensor < WRIST_ENC_MIN && wrist->power < 0) {wrist->power = 0;}
	if(claw->sensor > CLAW_POT_MAX && claw->power > 0) {claw->power = 0;}
	if(claw->sensor < CLAW_POT_MIN && claw->power < 0) {claw->power = 0;}

	blrsMotorSet(LIFT_MOTOR, scale(lift->power, LIFT_POWER_MIN, LIFT_POWER_MAX), true);
	blrsMotorSet(EXTEND_MOTOR, scale(extend->power, EXTEND_POWER_MIN, EXTEND_POWER_MAX), false);
	blrsMotorSet(WRIST_MOTOR, scale(wrist->power, WRIST_POWER_MIN, WRIST_POWER_MAX), false);
	blrsMotorSet(CLAW_MOTOR, scale(claw->power, CLAW_POWER_MIN, CLAW_POWER_MAX), false);
}

void armSet(ArmJoint joint, int power) {
	joints[joint].power = power;
}

int armGet(ArmJoint joint) {
	return joints[joint].sensor;
}

void armModPid(ArmJoint joint, double p, double i, double d) {
	joints[joint].pid.kP += p;
	joints[joint].pid.kI += i;
	joints[joint].pid.kD += d;
}

void armSetPid(ArmJoint joint, bool enabled) {
	if(enabled && !joints[joint].pidEnabled) {
		joints[joint].pidEnabled = true;
		fbcReset(&joints[joint].fbc);
		fbcSetGoal(&joints[joint].fbc, armGet(joint));
	}
	if(!enabled && joints[joint].pidEnabled) {
		joints[joint].pidEnabled = false;
	}
}

bool armGetPid(ArmJoint joint) {
	return joints[joint].pidEnabled;
}
void armSetTarget(ArmJoint joint, int target) {
	if(joints[joint].pidEnabled) {
		fbcSetGoal(&joints[joint].fbc, target);
	}
}

int armGetTarget(ArmJoint joint) {
	if(joints[joint].pidEnabled) {
		return joints[joint].fbc.goal;
	}
	return -1;
}
