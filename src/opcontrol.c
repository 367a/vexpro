/** @file opcontrol.c
 * @brief File for operator control code
 *
 * This file should contain the user operatorControl() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"

double getJoyWithThresh(unsigned char joystick, unsigned char axis, double threshold) {
	return applyDeadband(limit(joystickGetAnalog(joystick, axis)), threshold);
}

double mag;
double theta;
double wristHold;

void joyDebug(void) {
	//printf("m:%5.2f t:%5.2f\n", mag, theta);
}

void joyToChassis(void) {
	double y = -getJoyWithThresh(1, 3, 10);
	double x = getJoyWithThresh(1, 4, 10);
	double r = getJoyWithThresh(1, 1, 10);
//	double theta = atan2f(x, y) + PI/4.0f + PI;
//	double magnitude = (sqrtf(x*x+y*y) * (100.0f/127.0f)) - r;
	mag = sqrt(pow(x, 2) + pow(y, 2));
	theta = fmod(atan2(x, y) + PI/2.0f + PI, 2.0 * PI);

	if(buttonGetState(JOY1_5D)) {
		mag = mag / 4.0;
		r = r / 4.0;
	}
	chassisUseGyro(!buttonGetState(JOY1_6D));

	chassisDrivePolar(mag, theta, r);
	if(buttonIsNewPress(JOY1_8D)) {
		chassisGyroReset();
	}
}

void joyToArm(void) {
	int lift = getJoyWithThresh(2, 3, 15);
	if(lift == 0 && !armGetPid(LIFT)) {
		armSetPid(LIFT, true);
		armSetTarget(LIFT, armGet(LIFT));
	} else if(abs(lift) > 0) {
		armSetPid(LIFT, false);
		armSet(LIFT, lift);
	}

	int extend = getJoyWithThresh(2, 2, 15);
	armSet(EXTEND, extend);

	if(buttonGetState(JOY2_5U)) {
		armSetPid(WRIST, false);
		armSet(WRIST, -127);
	} else if(buttonGetState(JOY2_5D)) {
		armSetPid(WRIST, false);
		armSet(WRIST, 127);
	} else if(!armGetPid(WRIST)){
		armSet(WRIST, 0);
		armSetPid(WRIST, true);
		double scaled_lift_val2 = ((double) armGet(LIFT)- (double)LIFT_ENC_MIN)/(LIFT_ENC_MAX-LIFT_ENC_MIN);
		wristHold = armGet(WRIST) - (478.0 * scaled_lift_val2);
	}

	if(armGetPid(WRIST)) {
		double scaled_lift_val = ((double) armGet(LIFT)- (double)LIFT_ENC_MIN)/(LIFT_ENC_MAX-LIFT_ENC_MIN);
		armSetTarget(WRIST, wristHold+(478.0 * scaled_lift_val));
	}

	if(buttonGetState(JOY2_6U)) {
		armSetPid(CLAW, false);
		armSet(CLAW, -127);
	} else if(buttonGetState(JOY2_6D)) {
		armSetPid(CLAW, false);
		armSet(CLAW, 127);
	} else if(!armGetPid(CLAW)) {
		armSet(CLAW, 0);
		armSetPid(CLAW, true);
		armSetTarget(CLAW, armGet(CLAW));
	}
}

void updateAll(void) {
	armUpdate();
	chassisUpdate();
	displayUpdate();
}

/*
 * Runs the user operator control code. This function will be started in its own task with the
 * default priority and stack size whenever the robot is enabled via the Field Management System
 * or the VEX Competition Switch in the operator control mode. If the robot is disabled or
 * communications is lost, the operator control task will be stopped by the kernel. Re-enabling
 * the robot will restart the task, not resume it from where it left off.
 *
 * If no VEX Competition Switch or Field Management system is plugged in, the VEX Cortex will
 * run the operator control task. Be warned that this will also occur if the VEX Cortex is
 * tethered directly to a computer via the USB A to A cable without any VEX Joystick attached.
 *
 * Code running in this task can take almost any action, as the VEX Joystick is available and
 * the scheduler is operational. However, proper use of delay() or taskDelayUntil() is highly
 * recommended to give other tasks (including system tasks such as updating LCDs) time to run.
 *
 * This task should never exit; it should end with some kind of infinite loop, even if empty.
 */
void operatorControl() {
	buttonInit();
	while(1) {
		joyToChassis();
		joyToArm();
		updateAll();
		delay(20);
	}
}
