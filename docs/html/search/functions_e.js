var searchData=
[
  ['scale',['scale',['../main_8h.html#a510796ffe8e940b25ff9992f26a1ec86',1,'main.h']]],
  ['semaphorecreate',['semaphoreCreate',['../API_8h.html#a4461acf29574576dda6a3316117f85a9',1,'API.h']]],
  ['semaphoredelete',['semaphoreDelete',['../API_8h.html#af27ba79dc102f914d31a3c20136b1cd9',1,'API.h']]],
  ['semaphoregive',['semaphoreGive',['../API_8h.html#a9e5b0b6d5da138b4d5a077237894f96e',1,'API.h']]],
  ['semaphoretake',['semaphoreTake',['../API_8h.html#a7520baa9cf5c9ec2f43925b098e7b46a',1,'API.h']]],
  ['setteamname',['setTeamName',['../API_8h.html#a22269cefc22e487f7acdcc4737d58c4a',1,'API.h']]],
  ['snprintf',['snprintf',['../API_8h.html#ada81026ae730d990159aab26c302a3ad',1,'API.h']]],
  ['speakerinit',['speakerInit',['../API_8h.html#a7e0b8a79a6f53f88329b87229e7d698b',1,'API.h']]],
  ['speakerplayarray',['speakerPlayArray',['../API_8h.html#af91f9f80737d283ff82a96596f833854',1,'API.h']]],
  ['speakerplayrtttl',['speakerPlayRtttl',['../API_8h.html#a6971b95fa28048bf134b7421b7f2faee',1,'API.h']]],
  ['speakershutdown',['speakerShutdown',['../API_8h.html#a8d6d3ddc25b8408b0270cd2ccb9505ce',1,'API.h']]],
  ['sprintf',['sprintf',['../API_8h.html#acbfbfc380f865613ad5ff3cae256bdc4',1,'API.h']]],
  ['standalonemodeenable',['standaloneModeEnable',['../API_8h.html#a7bf146d0ac724624ae0147c8e225b713',1,'API.h']]]
];
