var searchData=
[
  ['wrist_5fenc_5fmax',['WRIST_ENC_MAX',['../ports_8h.html#a9eb3de24c0a4d3335c1c8165855b6a9b',1,'ports.h']]],
  ['wrist_5fenc_5fmin',['WRIST_ENC_MIN',['../ports_8h.html#a1b3fbfd3f5f69d24a4c171577a5a3f21',1,'ports.h']]],
  ['wrist_5fencoder_5fbottom',['WRIST_ENCODER_BOTTOM',['../ports_8h.html#a25c0ae29e9882ea1b9714a8e80989c85',1,'ports.h']]],
  ['wrist_5fencoder_5ftop',['WRIST_ENCODER_TOP',['../ports_8h.html#a8f99783f4e706691d17dc2bd36a9efbf',1,'ports.h']]],
  ['wrist_5fmotor',['WRIST_MOTOR',['../ports_8h.html#a91133b392be42a744863f9b08840e26f',1,'ports.h']]],
  ['wrist_5fpower_5fmax',['WRIST_POWER_MAX',['../ports_8h.html#adfe879af5d873edd0f01ac21886d28ee',1,'ports.h']]],
  ['wrist_5fpower_5fmin',['WRIST_POWER_MIN',['../ports_8h.html#ab316338e6a619f7cff062db5f1a961f6',1,'ports.h']]]
];
