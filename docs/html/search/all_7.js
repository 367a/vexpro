var searchData=
[
  ['getchar',['getchar',['../API_8h.html#ac45fdeab51c3197c1e7c4ec7beabaca9',1,'API.h']]],
  ['goal',['goal',['../structfbc.html#abae8f0c719ed402addcfe0c557a2185c',1,'fbc']]],
  ['gyro',['Gyro',['../API_8h.html#a04e06985633aa933343fcfa3d7fb268d',1,'API.h']]],
  ['gyroget',['gyroGet',['../API_8h.html#a0ae2ca5d2fd99f33aaef38786bb8ee59',1,'API.h']]],
  ['gyroinit',['gyroInit',['../API_8h.html#a17270080a32b64937a3669089a80120f',1,'API.h']]],
  ['gyroreset',['gyroReset',['../API_8h.html#a5de4afb9c6bd747e8d7664e1c72390b2',1,'API.h']]],
  ['gyroshutdown',['gyroShutdown',['../API_8h.html#a4e50e79b76d956dd9d466a582a5bb7b5',1,'API.h']]]
];
