var searchData=
[
  ['encoder',['Encoder',['../API_8h.html#a8289b20280bf9db1462f60dae76d2939',1,'API.h']]],
  ['encoderget',['encoderGet',['../API_8h.html#a5cfffd673e7fc8bcd1827f11b2b1490b',1,'API.h']]],
  ['encoderinit',['encoderInit',['../API_8h.html#aa68a1ba3d46d89bdb40961c52aa2c4d0',1,'API.h']]],
  ['encoderreset',['encoderReset',['../API_8h.html#a27500c21f56b2f44c62a9284ca5ebd44',1,'API.h']]],
  ['encodershutdown',['encoderShutdown',['../API_8h.html#ad068eaed82fe8c8f08ba02ea8eaf2d17',1,'API.h']]],
  ['eof',['EOF',['../API_8h.html#a59adc4c82490d23754cd39c2fb99b0da',1,'API.h']]],
  ['extend',['EXTEND',['../arm_8h.html#a899525fe4fef6c9ad06d1200bb51b37fa530337e96c40f8b760a351af9d5ece97',1,'arm.h']]],
  ['extend_5fmotor',['EXTEND_MOTOR',['../ports_8h.html#a752b0a525fe9cd23768ada9c3a698fa4',1,'ports.h']]],
  ['extend_5fpot_5fmax',['EXTEND_POT_MAX',['../ports_8h.html#af08991b4722e3e515533aa9d4203d55d',1,'ports.h']]],
  ['extend_5fpot_5fmin',['EXTEND_POT_MIN',['../ports_8h.html#a5fe199b2a2131de137e0f58dcac6319d',1,'ports.h']]],
  ['extend_5fpoten',['EXTEND_POTEN',['../ports_8h.html#a1ead2fa2d8f8dcc2aecddf9c74ae08d1',1,'ports.h']]],
  ['extend_5fpower_5fmax',['EXTEND_POWER_MAX',['../ports_8h.html#a1cd9a32c9a99cf8791c7e031fc850ebf',1,'ports.h']]],
  ['extend_5fpower_5fmin',['EXTEND_POWER_MIN',['../ports_8h.html#ae6a57670765e075d5c7c6795b57e179b',1,'ports.h']]]
];
