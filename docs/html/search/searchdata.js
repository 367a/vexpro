var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuw",
  1: "fm",
  2: "abcdfmp",
  3: "_abcdefgijlmopstuw",
  4: "_acfgiklmnoprs",
  5: "aefgimpstu",
  6: "ab",
  7: "cejlw",
  8: "abcdefhijlmnopstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

