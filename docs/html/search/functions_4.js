var searchData=
[
  ['delay',['delay',['../API_8h.html#a1c59207742a1acf45a8957d7f04f9dfe',1,'API.h']]],
  ['delaymicroseconds',['delayMicroseconds',['../API_8h.html#abee651cde0a0e6ed0df34c86ed5af756',1,'API.h']]],
  ['digitalread',['digitalRead',['../API_8h.html#a7321930f297f38e246050f7f5b091722',1,'API.h']]],
  ['digitalwrite',['digitalWrite',['../API_8h.html#a23e767e5b47fa61d4e2cc02e6f15c7ab',1,'API.h']]],
  ['displayinit',['displayInit',['../display_8h.html#a8c548751802c5960b3092be28cc56d9f',1,'display.h']]],
  ['displayregisterapp',['displayRegisterApp',['../display_8h.html#a010545bca3e232660ef6b977d302e905',1,'display.h']]],
  ['displayupdate',['displayUpdate',['../display_8h.html#a7c8251a4f5cb68a4454db946e8e6467e',1,'display.h']]]
];
