var searchData=
[
  ['eof',['EOF',['../API_8h.html#a59adc4c82490d23754cd39c2fb99b0da',1,'API.h']]],
  ['extend_5fmotor',['EXTEND_MOTOR',['../ports_8h.html#a752b0a525fe9cd23768ada9c3a698fa4',1,'ports.h']]],
  ['extend_5fpot_5fmax',['EXTEND_POT_MAX',['../ports_8h.html#af08991b4722e3e515533aa9d4203d55d',1,'ports.h']]],
  ['extend_5fpot_5fmin',['EXTEND_POT_MIN',['../ports_8h.html#a5fe199b2a2131de137e0f58dcac6319d',1,'ports.h']]],
  ['extend_5fpoten',['EXTEND_POTEN',['../ports_8h.html#a1ead2fa2d8f8dcc2aecddf9c74ae08d1',1,'ports.h']]],
  ['extend_5fpower_5fmax',['EXTEND_POWER_MAX',['../ports_8h.html#a1cd9a32c9a99cf8791c7e031fc850ebf',1,'ports.h']]],
  ['extend_5fpower_5fmin',['EXTEND_POWER_MIN',['../ports_8h.html#ae6a57670765e075d5c7c6795b57e179b',1,'ports.h']]]
];
