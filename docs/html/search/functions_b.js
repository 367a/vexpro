var searchData=
[
  ['micros',['micros',['../API_8h.html#a8b24cbb7c3486e1bfa05c86db83ecb01',1,'API.h']]],
  ['millis',['millis',['../API_8h.html#a6ff7f2532a22366f0013bc41397129fd',1,'API.h']]],
  ['motorget',['motorGet',['../API_8h.html#a4805c8fd29f9221d28ed2e673c06e6c4',1,'API.h']]],
  ['motormanagerinit',['motorManagerInit',['../mtrmgr_8h.html#a9468a8afc4938a160037a32980ce03fd',1,'mtrmgr.h']]],
  ['motormanagerstop',['motorManagerStop',['../mtrmgr_8h.html#afe681ac50931cc114c2b03ef9a91701b',1,'mtrmgr.h']]],
  ['motorset',['motorSet',['../API_8h.html#a03c5b04b472d024281f62d7af8854a8e',1,'API.h']]],
  ['motorstop',['motorStop',['../API_8h.html#a339844ebc35f48a14945b73edaeca498',1,'API.h']]],
  ['motorstopall',['motorStopAll',['../API_8h.html#a8966c541f3e9565aea1289f0d2f2cf43',1,'API.h']]],
  ['mutexcreate',['mutexCreate',['../API_8h.html#aecd027ce8f8b52a765735e9eb5b202b3',1,'API.h']]],
  ['mutexdelete',['mutexDelete',['../API_8h.html#a247598f8083a3ce6c39317d279f631cf',1,'API.h']]],
  ['mutexgive',['mutexGive',['../API_8h.html#afe171a08d22de18fc2ab604b2364959f',1,'API.h']]],
  ['mutextake',['mutexTake',['../API_8h.html#a8b51124628d2a7741738d48551d1e8ee',1,'API.h']]]
];
